package com.example.latihancalc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.latihancalc.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var inputan1 = 0
    private var inputan2 = 0
    private var inResult = 0
    private var operatorType = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        val tambah = 1
//        val kurang = 2
//        val kali  = 3
//        val bagi = 4

        binding.btnTambah.setOnClickListener {
            changeOperator("+")
            operatorType = OperatorType.TAMBAH
        }
        binding.btnKurang.setOnClickListener {
            changeOperator("-")
            operatorType = OperatorType.KURANG
        }
        binding.btnKali.setOnClickListener {
            changeOperator("*")
            operatorType = OperatorType.KALI
        }
        binding.btnBagi.setOnClickListener {
            changeOperator("/")
            operatorType = OperatorType.BAGI
        }
        binding.btnProses.setOnClickListener {
            calculate()
        }
    }

    private fun changeOperator(operator: String){
        binding.tvOperator.text = operator
    }
    private fun tambah(){
        inResult = inputan1 + inputan2
    }
    private fun kurang(){
        inResult = inputan1 - inputan2
    }
    private fun kali(){
        inResult = inputan1 * inputan2
    }
    private fun bagi() {
        inResult = inputan1 / inputan2
    }
    private fun calculate(){
        inputan1 = binding.input1.text.toString().toInt()
        inputan2 = binding.input2.text.toString().toInt()

        when (operatorType){
            OperatorType.TAMBAH -> {
                //penambahan
                tambah()
            }
            OperatorType.KURANG -> {
                //pengurangan
                kurang()
            }
            OperatorType.KALI -> {
                //perkalian
                kali()
            }
            OperatorType.BAGI -> {
                //pembagian
                bagi()
            }
        }
        binding.tvResult.text = inResult.toString()
    }
}