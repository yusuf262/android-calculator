package com.example.latihancalc

object OperatorType {
    const val TAMBAH = 1
    const val KURANG = 2
    const val KALI = 3
    const val BAGI = 4
}